var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var cors = require("cors");

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// app.get("http://196.168.1.6:3000", (req, res) => {
//   console.log(req.body);
// });
var request = require("request");
request("http://192.168.1.6:8086/msaccess/dtr", (error, response, body) => {
  if (!error && response.statusCode == 200) {
    app.get("/", (req, res) => {
      console.log(body);
      res.send(body);
    });
  }
});
app.post("/login", function(request, response) {
  console.log(request.body);
  response.send(request.body);
});

app.listen("8080");
