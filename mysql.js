var mysql = require("mysql");

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "dtr_db"
});

con.connect(function(error) {
  if (error) {
    throw error;
  }

  console.log("Connected");
  var sql = "SELECT * FROM person";
  con.query(sql, function(error, result) {
    if (error) throw error;
    console.log(result);
  });
});
